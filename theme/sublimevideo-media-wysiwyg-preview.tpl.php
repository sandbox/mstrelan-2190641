<?php
/**
 * @file
 * Provide a stub HTML output of the SublimeVideo video player for WYSIWYG editors.
 *
 * Available variables:
 * Same as videojs.tpl.php.
 */
?>
<img src="<?php print $poster['safe']; ?>" width="<?php print $width; ?>" height="<?php print $height; ?>" />
