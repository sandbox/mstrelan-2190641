<?php
/**
 * @file
 * Administrative pages for the SublimeVideo module.
 */

/**
 * Menu callback; Provides the SublimeVideo settings form.
 */
function sublimevideo_settings_form() {
  $form = array();

  $form['sublimevideo_site_token'] = array(
    '#type' => 'textfield',
    '#title' => t('SublimeVideo site\'s token'),
    '#description' => t('Get the site\'s token form your SublimeVideo account at !link', array(
      '!link' => l('https://my.sublimevideo.net/sites', 'https://my.sublimevideo.net/sites'),
    )),
    '#default_value' => variable_get('sublimevideo_site_token', ''),
    '#size' => 40,
    '#maxlength' => 90,
    '#required' => TRUE,
  );

  $form['sublimevideo_quality_hd'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum HD height'),
    '#description' => t('The minimum height in pixels that can be classified as HD. Requires !getid3.', array(
      '!getid3' => l('getID3', 'http://drupal.org/project/getid3'),
    )),
    '#default_value' => variable_get('sublimevideo_quality_hd', '720'),
    '#size' => 4,
    '#maxlength' => 5,
    '#required' => TRUE,
  );

  $form['location'] = array(
    '#type' => 'fieldset',
    '#title' => t('SublimeVideo location'),
    '#collapsible' => FALSE,
  );

  $locations = array(
    'cdn' => t('SublimeVideo Content Delivery Network (CDN)'),
  );

  $form['location']['sublimevideo_location'] = array(
    '#type' => 'select',
    '#title' => t('SublimeVideo location'),
    '#options' => $locations,
    '#default_value' => variable_get('sublimevideo_location', 'cdn'),
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Player defaults'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  sublimevideo_utility::getDisplaySettingsForm($form['options']);
  $form['#submit'][] = 'sublimevideo_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for the sublimevideo_settings_form() form.
 */
function sublimevideo_settings_form_submit($form, &$form_state) {
  $options = sublimevideo_utility::getDisplaySettingsFormResults($form_state['values']['options']);
  unset($form_state['values']['options']);

  // Add the results to the form state so they will be saved by the system
  // settings form submit handler.
  foreach ($options as $k => $v) {
    if (!empty($v)) {
      $form_state['values']['sublimevideo_' . $k] = $v;
    }
    else {
      variable_del('sublimevideo_' . $k);
    }
  }
}
